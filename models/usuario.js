const mongoose = require('mongoose');
const Reserva = require('./reserva');
const bcrypt = require('bcrypt');
const crypto = require('crypto');
const uniqueValidator = require('mongoose-unique-validator');
const saltRounds = 10; // para agregar aletoriedad

const Token = require('../models/token');
const mailer = require('../mailer/mailer');

const Schema = mongoose.Schema;


function validateEmail(email) {
    const re = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
    return re.test(email);
}

const usuarioSchema = new Schema({
    nombre: {
        type: String,
        trim: true,
        required: [true, "El nombre es obligatorio"]
    },
    email: {
        type: String,
        trim: true,
        required: [true, "El email es obligatorio"],
        lowercase: true,
        unique: true,
        validate: [validateEmail, 'Ingrese un email válido'],
        match: [/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/],
    },
    password: {
        type: String,
        required: [true, "El password es obligatorio"],
    },
    passwordResetToken: String,
    passwordResetTokenExpires: Date,
    verificado: {
        type: Boolean,
        default: false,
    },
    googleId: String,
    facebookId: String,
});

usuarioSchema.plugin(uniqueValidator, {message: 'El {PATH} ya existe con otro usuario'});

usuarioSchema.pre('save', function(next){
    if(this.isModified('password')){
        this.password = bcrypt.hashSync(this.password, saltRounds);
    }
    next();
});

usuarioSchema.methods.validPassword = function(password){
    return bcrypt.compareSync(password, this.password);
}

usuarioSchema.methods.reservar = function(biciId, desde, hasta, cb){
    var reserva = new Reserva({
        usuario: this._id,
        bicicleta: biciId,
        desde: desde,
        hasta: hasta,
    });
    reserva.save(cb);
}

usuarioSchema.methods.enviar_email_bienvenida = function(cb){
    const token = new Token({_userId: this.id, token: crypto.randomBytes(16).toString('hex')});
    const email_destination = this.email;

    token.save(function(err){
        if(err) return console.log(err.message);

        const mailOptions = {
            from: 'no-reply@redbicicletas.com',
            to: email_destination,
            subject: 'Verificación de cuenta',
            html: 'Hola, <br><br> Por favor, para verificar su cuenta haga clic en este enlace: <br> <a href="http://localhost:3000/token/confirmation/'+token.token+'">http://localhost:3000/token/confirmation/'+token.token+'</a>.'
        };

        mailer.sendMail(mailOptions, function(err){
            if(err) return console.log(err.message);

            console.log('Enviado email de bienvenida a ', email_destination);
        });
    });
}

usuarioSchema.methods.resetPassword = function(cb){
    const token = new Token({_userId: this.id, token: crypto.randomBytes(16).toString('hex')});
    const email_destination = this.email;

    token.save(function(err){
        if(err) return console.log(err.message);

        const mailOptions = {
            from: 'no-reply@redbicicletas.com',
            to: email_destination,
            subject: 'Recuperación de contraseña',
            html: 'Hola, <br><br> Para cambiar su contraseña haga clic en este enlace: <br> <a href="http://localhost:3000/resetPassword/'+token.token+'">http://localhost:3000/resetPassword/'+token.token+'</a>.'
        };

        mailer.sendMail(mailOptions, function(err){
            if(err) return console.log(err.message);

            console.log('Enviado email de cambio de contraseña a ', email_destination);
        });
    });
}

usuarioSchema.statics.findOneOrCreateByGoogle = function(condition, callback){
    const self = this;
    console.log("--CONDITION--");
    console.log(condition);
    self.findOne({
        $or: [
            {'googleId': condition.id}, {'email': condition.emails[0].value}
        ]},
        function(err, result){
            if(result){
                callback(err, result);
            }
            else{
                let values = {};
                values.googleId = condition.id;
                values.email = condition.emails[0].value;
                values.nombre = condition.displayName || 'Sin nombre';
                values.verificado = true;
                values.password = condition._json.sub;
                console.log("--VALUES--");
                console.log(values);
                self.create(values, function(err, result){
                    if(err) console.log(err);
                    return callback(err, result);
                });
            }
        }
    );
}

usuarioSchema.statics.findOneOrCreateByFacebook = function(condition, callback){
    const self = this;
    console.log("--CONDITION--");
    console.log(condition);
    self.findOne({
        $or: [
            {'facebookId': condition.id}, {'email': condition.emails[0].value}
        ]},
        function(err, result){
            if(result){
                callback(err, result);
            }
            else{
                let values = {};
                values.facebookId = condition.id;
                values.email = condition.emails[0].value;
                values.nombre = condition.displayName || 'Sin nombre';
                values.verificado = true;
                values.password = crypto.randomBytes(16).toString('hex');
                console.log("--VALUES--");
                console.log(values);
                self.create(values, function(err, result){
                    if(err) console.log(err);
                    return callback(err, result);
                });
            }
        }
    );
}

module.exports = mongoose.model('Usuario', usuarioSchema);