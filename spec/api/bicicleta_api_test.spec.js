const Bicicleta = require('../../models/bicicleta');
const request = require('request');
const server = require('../../bin/www');

beforeEach(() => { console.log('testeando…'); })

describe('Bicicleta API', () => {

    describe('GET Bicicletas /', () => {
        it('status 200', (done) => {

            const bici = new Bicicleta({code: 1, color: 'rojo', modelo: 'urbana', ubicacion: [-34.6012424, -58.3861497]});
            Bicicleta.add(bici, function(err, newBici){
                request.get('http://localhost:3000/api/bicicletas', function(error, response, body){
                    expect(response.statusCode).toBe(200);
                    done();
                });
            });
        });
    });
    
    describe('POST Bicicletas /create', () => {
        it('status 200', (done) => {
            const headers = {"content-type": "application/json"};
            const bici = '{"id": 6, "color": "blanco", "modelo": "urbana", "latitud": -11, "longitud": -54}';

            request.post({
                headers: headers,
                url: 'http://localhost:3000/api/bicicletas/create/',
                body: bici,
            }, function(error, response, body){
                expect(response.statusCode).toBe(200);
                Bicicleta.findByCode(6, function(err, bici){
                    expect(bici.color).toBe("blanco");
                    done();
                })
            })
        })
    });
});