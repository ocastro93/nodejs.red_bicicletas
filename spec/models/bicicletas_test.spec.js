const mongoose = require('mongoose');
const Bicicleta = require('../../models/bicicleta');

describe('Testing bicicletas', () => {
    beforeAll(function(done){
        const mongodb = "mongodb://localhost/testdb";
        mongoose.connect(mongodb, {useNewUrlParser: true});

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, "connection failed"));
        db.once('open', function(){
            console.log("Connected to the db!");
            done();
        });
    });

    afterEach(function(done){
        Bicicleta.deleteMany({}, function(err, success){
            if(err) console.log(err);
            done();
        });
    });

    afterAll(function(){
        mongoose.connection.close();
    });

    describe('Bicicleta.createInstance', () => {
        it('crea una instancia de bicicleta', () => {
            const bici = Bicicleta.createInstance(1, "verde", "playa", [-34.5, -54.1]);

            expect(bici.code).toBe(1);
            expect(bici.color).toBe("verde");
            expect(bici.modelo).toBe("playa");
            expect(bici.ubicacion[0]).toEqual(-34.5);
            expect(bici.ubicacion[1]).toEqual(-54.1);
        })
    });

    describe('Bicicleta.allBicis', () => {
        it('comienza vacia', (done) => {
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0);
                done();
            });
        });
    });

    describe('Bicicleta.add', () => {
        it('agregar una bici', (done) => {
            
            const bici = new Bicicleta({code: 1, color: 'rojo', modelo: 'urbana'});
            Bicicleta.add(bici, function(err, newBici){
                if(err) console.log(err);
                Bicicleta.allBicis(function(err, bicis){
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(bici.code);

                    done();
                });
            });
        });
    });
    
    describe('Bicicleta.findByCode', () => {
        it('devuelve la bici con el codigo 1', (done) => {

            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toEqual(0);
            
                const bici = new Bicicleta({code: 1, color: 'rojo', modelo: 'urbana'});
                Bicicleta.add(bici, function(err, newBici){
                    if(err) console.log(err);

                    const bici2 = new Bicicleta({code: 2, color: 'verde', modelo: 'playa'});
                    Bicicleta.add(bici2, function(err, newBici){
                        if(err) console.log(err);

                        Bicicleta.findByCode(1, function(error, targetBici){
                            expect(targetBici.code).toEqual(bici.code);
                            expect(targetBici.color).toEqual(bici.color);
                            expect(targetBici.modelo).toEqual(bici.modelo);

                            done();
                        });
                    });
                });
            });
        });
    });

});