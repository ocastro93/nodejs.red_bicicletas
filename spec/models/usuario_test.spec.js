const mongoose = require('mongoose');
const Bicicleta = require('../../models/bicicleta');
const Usuario = require('../../models/usuario');
const Reserva = require('../../models/reserva');

describe('Testing usuario', () => {
    beforeAll(function(done){
        const mongodb = "mongodb://localhost/testdb";
        mongoose.connect(mongodb, {useNewUrlParser: true});

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, "connection failed"));
        db.once('open', function(){
            console.log("Connected to the db!");
            done();
        });
    });

    afterEach(function(done){
        Reserva.deleteMany({}, function(err, success){
            if(err) console.log(err);
            Usuario.deleteMany({}, function(err, success){
                if(err) console.log(err);
                Bicicleta.deleteMany({}, function(err, success){
                    if(err) console.log(err);
                    done();
                });
            });
        });
    });

    afterAll(function(){
        mongoose.connection.close()
    });

    describe('Usuario reserva una bicicleta', () => {
        it('probar proceso de reserva', () => {
            const usuario = new Usuario({nombre: "Pedro"});
            usuario.save();
            const bici = new Bicicleta({code: 1, color: 'rojo', modelo: 'urbana'});
            bici.save();

            const hoy = new Date();
            const manana = new Date();
            manana.setDate(hoy.getDate()+1);

            usuario.reservar(bici.id, hoy, manana, function(err, reserva){
                Reserva.find({}).populate('bicicleta').populate('usuario').exec(function(err, reservas){
                    console.log(reservas[0]);
                    expect(reservas.length).toBe(1);
                    expect(reservas[0].diasDeReserva()).toBe(2);
                    expect(reservas[0].bicicleta.code).toBe(1);
                    expect(reservas[0].usuario.nombre).toBe(usuario.nombre);
                    done(); 
                });
            });

        })
    });

    

});