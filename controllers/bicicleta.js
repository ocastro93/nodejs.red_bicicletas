const Bicicleta = require('../models/bicicleta');

exports.bicicleta_list = function(req, res, next){
    Bicicleta.allBicis(function(err, bicis){
        res.render('bicicletas/index', {bicis: bicis});
    });
}

exports.bicicleta_create = function(req, res){
    res.render('bicicletas/create', {errors: {}, bicicleta: new Bicicleta()});
}

exports.bicicleta_create_post = function(req, res){
    const bici = new Bicicleta({code: req.body.code, color: req.body.color, modelo: req.body.modelo, ubicacion: [req.body.latitud, req.body.longitud]});
    Bicicleta.add(bici, function(err, newBici){
        if(err){
            res.render('bicicletas/create', {errors: err.errors, bicicleta: bici});
        }
        else{
            res.redirect('/bicicletas');
        }
    });
}

exports.bicicleta_update = function(req, res){
    Bicicleta.findByCode(req.params.code, function(error, targetBici){
        res.render('bicicletas/update', {bici: targetBici});
    });
}

exports.bicicleta_update_post = function(req, res){
    const query = {'code': req.params.code};
    const update_values = {code: req.body.code, color: req.body.color, modelo: req.body.modelo, ubicacion: [req.body.latitud, req.body.longitud]}
    Bicicleta.findOneAndUpdate(query, update_values, function(err, bici){
        if(err){
            console.log(err);
            res.render('bicicletas/update', {errors: err.errors, bicicleta: new Usuario(update_values)});
        }
        else{
            res.redirect('/bicicletas');
            return;
        }
    })
}

exports.bicicleta_delete_post = function(req, res){
    Bicicleta.findOneAndDelete({'code': req.params.code}, function(err){
        if(err){
            next(err);
        }
        else{
            res.redirect('/bicicletas');
        }
    });
}