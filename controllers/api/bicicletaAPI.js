const Bicicleta = require('../../models/bicicleta');

exports.bicicleta_list = function(req, res){
    Bicicleta.allBicis(function(err, bicis){
        res.status(200).json(bicis)
    });
}

exports.bicicleta_create = function(req, res){
    const bici = new Bicicleta({
        code: req.body.id, 
        color: req.body.color, 
        modelo: req.body.modelo, 
        ubicacion: [req.body.longitud, req.body.latitud],
    });
    Bicicleta.add(bici, function(err){
        res.status(200).json(bici);
    });
}

exports.bicicleta_delete = function(req, res){
    Bicicleta.removeByCode(req.body.id, function(err){
        res.status(204).send();
    });
}