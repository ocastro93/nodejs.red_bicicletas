const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const Usuario = require('../../models/usuario');
const { usuario_reservar } = require('./usuarioAPI');

module.exports = {
    authenticate: function(req, res, next){
        Usuario.findOne({email: req.body.email}, function(err, usuario){
            if(err){
                next(err);
            }
            else{
                if (usuario === null) return res.status(401).json({status: "error", message: "Credenciales invalidas", data: null});

                if(usuario != null && bcrypt.compareSync(req.body.password, usuario.password)){
                    const token = jwt.sign({id: usuario._id}, req.app.get('secretKey'), {expiresIn: '7d'});
                    res.status(200).json({usuario: usuario, token: token});
                }
                else{
                    res.status(401).json({status: "error", message: "Credenciales invalidas", data: null});
                }
            }
        });
    },
    forgotPassword: function(req, res, next){
        Usuario.findOne({email: req.body.email}, function(err, usuario){
            if (!userInfo) return res.status(401).json({status: "error", message: "No existe el usuario", data: null});

            usuario.resetPassword(function(err){
                if(err) return next(err);
                res.status(200).json({message: "Se ha enviado un email para reestablecer la contraseña"});
            });
        });
    },
    authFacebookToken: function(req, res, next){
        if(req.user){
            req.user.save().then(function(){
                const token = jwt.sign({id: req.user.id}, req.app.get('secretKey'), {expiresIn: '7d'});
                res.status(200).json({usuario: req.user, token: token});
            }).catch(function(err){
                console.log(err);
                res.status(200).json({message: err.message});
            });
        }
    }
}