const express = require('express');
const router = express.Router();
const bicicletaController = require('../controllers/bicicleta');

router.get('/', bicicletaController.bicicleta_list);
router.get('/create', bicicletaController.bicicleta_create);
router.post('/create', bicicletaController.bicicleta_create_post);
router.get('/:code/update', bicicletaController.bicicleta_update);
router.post('/:code/update', bicicletaController.bicicleta_update_post);
router.post('/:code/delete', bicicletaController.bicicleta_delete_post);

module.exports = router;