var mymap = L.map('main_map').setView([-34.6012424, -58.381497], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors',
}).addTo(mymap);

// L.marker([-34.6012424, -58.381497]).addTo(mymap);

$.ajax({
    dataType: 'json',
    url: 'api/bicicletas',
    success: function(result){
        result.bicicletas.forEach(bici => {
            L.marker(bici.ubicacion, {title: bici.id}).addTo(mymap);
        });
    }
});